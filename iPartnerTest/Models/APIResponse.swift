//
//  APIResonse.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation

enum APIResponseStatus: Int {
    case failure = 0
    case success = 1
}

struct APIResponse {
    
    var response: Any?
    var status: APIResponseStatus
    var errorMessage: String?
    
    init?(from json: Any) {
        guard
            let json = json as? [String: Any],
            let status = json["status"] as? Int
            else { return nil }
        self.status = APIResponseStatus(rawValue: status) ?? .failure
        self.response = json["data"]
        self.errorMessage = json["error"] as? String
    }
}


