//
//  CompletionResult.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

public enum Result<T> {
    case success(T)
    case failure(Error)
}
