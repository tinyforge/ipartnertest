import Foundation

class User {
    
    // MARK: - Variables
    var sessionId: String {
        get { return UserDefaults.standard.string(forKey: UserDefaultsKey.SessionId) ?? "" }
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKey.SessionId)
            UserDefaults.standard.synchronize()
        }
    }
    
    // MARK: - Singleton
    static let shared = User()
    private init() {}
    
}

