//
//  Entry.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//
import Foundation

struct Entry {
    
    var id: String
    var text: String
    var createdAt: Date
    var updatedAt: Date

    init?(json: [String: Any]) {
        guard
            let id = json["id"] as? String,
            let text = json["body"] as? String,
            let createdAt = json["da"] as? String,
            let updatedAt = json["dm"] as? String
            else {  return nil }
        self.id = id
        self.text = text
        self.createdAt = Date(timeIntervalSince1970: TimeInterval(createdAt) ?? 0)
        self.updatedAt = Date(timeIntervalSince1970: TimeInterval(updatedAt) ?? 0)
    }
    
    static func getArray(from jsonArray: Any) -> [Entry]? {
        guard
            let jsonArray = jsonArray as? Array<Array<[String: Any]>>
            else { return nil }
        let entryArray = jsonArray.flatMap { $0 }
        return entryArray.compactMap { Entry(json: $0) }
    }
}
