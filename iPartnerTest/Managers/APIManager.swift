//
//  APIManager.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation
import Alamofire

class APIManager {

    typealias EntryId = String
    typealias SessionId = String
    
    class var isConnectedToInternet: Bool { return NetworkReachabilityManager()!.isReachable }
    var session: String
    
    init(session: String) {
        self.session = session
    }
    
    func createNewSession(completion: @escaping(Result<SessionId>) -> Void) {
        let parameters = ["a": "new_session"]
        self.postRequest(parameters: parameters) { result in
            switch result {
            case .success(let response):
                guard
                    let json = response as? [String: Any],
                    let sessionId = json["session"] as? String
                    else {
                        let error = NSError(code: 9999, localizedDescription: "Can not get sessionId from JSON")
                        return completion(.failure(error))
                }
                completion(.success(sessionId))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func getEntries(completion: @escaping(Result<[Entry]>) -> Void) {
        let parameters = [
            "a": "get_entries",
            "session": session
        ]
        self.postRequest(parameters: parameters) { result in
            switch result {
            case .success(let response):
                guard let entries = Entry.getArray(from: response) else {
                    let error = NSError(code: 9999, localizedDescription: "Can not create Entry Array from JSON")
                    return completion(.failure(error))
                }
                completion(.success(entries))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func addEntry(body: String, completion: @escaping(Result<EntryId>) -> Void) {
        let parameters = [
            "a": "add_entry",
            "session": session,
            "body": body
        ]
        self.postRequest(parameters: parameters) { result in
            switch result {
            case .success(let response):
                guard
                    let json = response as? [String: Any],
                    let entryId = json["id"] as? String
                    else {
                        let error = NSError(code: 9999, localizedDescription: "Can not get entryId from JSON")
                        return completion(.failure(error))
                }
                completion(.success(entryId))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func postRequest(parameters: [String: String], completion: @escaping(Result<Any>) -> Void) {
        let httpHeaders = ["token": API.token]
        request(API.url, method: .post, parameters: parameters, headers: httpHeaders).validate().responseJSON { responseJSON in
            switch responseJSON.result {
            case .success(let response):
                guard let apiResponse = APIResponse(from: response) else {
                    let error = NSError(code: 9999, localizedDescription: "Can not create APIResponse model from JSON")
                    return completion(.failure(error))
                }
                if apiResponse.status == .failure {
                    let error = NSError(code: 9999, localizedDescription: apiResponse.errorMessage ?? "Unknown error")
                    return completion(.failure(error))
                }
                completion(.success(apiResponse.response as Any))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}
