//
//  UserManager.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation

class DatastoreManager: NSObject {
    
    // MARK: - Session Id
    class func loadSessionId() -> String? {
        return UserDefaults.standard.string(forKey: UserDefaultsKey.SessionId)
    }
    
    class func saveSessionId(_ sessionId: String?) {
        UserDefaults.standard.set(sessionId, forKey: UserDefaultsKey.SessionId)
        UserDefaults.standard.synchronize()
    }
    
}
