import UIKit

extension UIView {
    
    // MARK: - XIB
    func loadViewFromNib() -> UIView? {
        guard let nibName = String(describing: type(of: self)).components(separatedBy: ".").last else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    // MARK: - Constraints
    func constrainToEdges(to other: UIView, padding: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: other.leadingAnchor, constant: padding).isActive = true
        trailingAnchor.constraint(equalTo: other.trailingAnchor, constant: -padding).isActive = true
        topAnchor.constraint(equalTo: other.topAnchor, constant: padding).isActive = true
        bottomAnchor.constraint(equalTo: other.bottomAnchor, constant: -padding).isActive = true
    }

}

