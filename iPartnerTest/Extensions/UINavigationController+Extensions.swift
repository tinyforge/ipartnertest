import UIKit

extension UINavigationController {
    var previuosViewController: UIViewController? {
            let lenght = self.viewControllers.count
            let previousViewController: UIViewController? = lenght >= 2 ? self.viewControllers[lenght-2] : nil
            return previousViewController
    }
}
