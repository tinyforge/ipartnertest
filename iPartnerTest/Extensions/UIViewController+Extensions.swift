import UIKit

extension UIViewController {
    
    // Return UIViewController from storyboard
    class func storyboardInstance() -> Self {
        return storyboardInstancePrivate()
    }
    private class func storyboardInstancePrivate<T: UIViewController>() -> T {
        let storyboardName = String(describing: self).replacingOccurrences(of: "ViewController", with: "")
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateInitialViewController() as! T
    }

    // Alert
    func showAlert(title: String, message: String, completion: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .cancel) { _ in completion?() }
        alertController.addAction(OKAction)
        DispatchQueue.main.async { [weak self] in
            self?.present(alertController, animated: true, completion: nil)
        }
    }

}
