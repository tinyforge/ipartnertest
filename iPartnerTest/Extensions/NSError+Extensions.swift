import Foundation

extension NSError {
    
    public convenience init(code: Int, localizedDescription: String) {
        let domain = Bundle.main.bundleIdentifier!
        self.init(domain: domain, code: code, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
    }
    
}
