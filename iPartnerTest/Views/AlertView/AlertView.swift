//
//  AlertView.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 05/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol AlertViewDelegate: class {
    func buttonTap(_ sender: UIButton)
}

class AlertView: UIView {
    
    weak var delegate: AlertViewDelegate?
    private var view: UIView?
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    // MARK: Object lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    private func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        self.view = view
    }
    
    @IBAction private func buttonTap(_ sender: UIButton) {
        delegate?.buttonTap(sender)
    }
    
    // MARK: - Methods
    func show(in view: UIView) {
        view.addSubview(self)
        self.constrainToEdges(to: view)
    }
    
    func remove() {
        self.removeFromSuperview()
    }
    

}
