//
//  ActivityIndicatorView.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 04/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

class ActivityIndicatorView: UIView {
    
    private var loadingView = UIView()
    private var activityIndicator = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(in view: UIView) {
        self.frame = view.frame
        self.center = view.center
        self.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 64, height: 64)
        loadingView.center = view.center
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 8
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 32, height: 32);
        activityIndicator.style = .whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width  / 2,
                                           y: loadingView.frame.size.height / 2)
        
        loadingView.addSubview(activityIndicator)
        addSubview(loadingView)
        view.addSubview(self)
        
        activityIndicator.startAnimating()
    }
    
    func hide() {
        self.removeFromSuperview()
    }

}
