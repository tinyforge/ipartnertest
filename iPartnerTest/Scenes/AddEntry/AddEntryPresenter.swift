//
//  AddEntryPresenter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol AddEntryBusinessLogic: NSObjectProtocol {
    func saveEntry(text: String?)
}

class AddEntryPresenter: NSObject {
    weak var view: AddEntryDisplayLogic?
    var router: AddEntryRoutingLogic
    init(view: AddEntryDisplayLogic, router: AddEntryRoutingLogic) {
        self.view = view
        self.router = router
    }
}

extension AddEntryPresenter: AddEntryBusinessLogic {
    func saveEntry(text: String?) {
        guard let text = text, !text.isEmpty else {
            view?.showAlert(title: "Alert", message: "Введите текст")
            return
        }

        view?.startLoading()
        let apiManager = APIManager(session: User.shared.sessionId)
        apiManager.addEntry(body: text) { [weak self] result in
            guard let self = self else { return }
            self.view?.stopLoading()
            switch result {
            case .success:
                self.router.navigateToParentWithUpdate()
            case .failure(let error):
                self.view?.showAlert(title: "Alert", message: error.localizedDescription)
            }
        }
    }

}
