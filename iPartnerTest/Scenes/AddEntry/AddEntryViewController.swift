//
//  AddEntryViewController.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol AddEntryDisplayLogic: NSObjectProtocol {
    func startLoading()
    func stopLoading()
    func showAlert(title: String, message: String)
}

class AddEntryViewController: UIViewController {
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            textView.delegate = self
        }
    }
    var presenter: AddEntryBusinessLogic!
    var activitiIndicatorView = ActivityIndicatorView()
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    func setup() {
        let router = AddEntryRouter(viewController: self)
        presenter = AddEntryPresenter(view: self, router: router)
    }
    
    // MARK: Viwe lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.becomeFirstResponder()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if self.view.isUserInteractionEnabled {
            presenter.saveEntry(text: textView.text)
        }
    }

}

extension AddEntryViewController: AddEntryDisplayLogic {
    func showAlert(title: String, message: String) {
        super.showAlert(title: title, message: message)
    }
    
    func startLoading() {
        self.view.isUserInteractionEnabled = false
        activitiIndicatorView.show(in: self.view)
    }
    
    func stopLoading() {
        activitiIndicatorView.hide()
        self.view.isUserInteractionEnabled = true
    }
}

extension AddEntryViewController: UITextViewDelegate {
    
}
