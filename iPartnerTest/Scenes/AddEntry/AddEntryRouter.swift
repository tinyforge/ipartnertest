//
//  AddEntryRouter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 05/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation

protocol AddEntryRoutingLogic {
    func navigateToParentWithUpdate()
}

class AddEntryRouter {
    weak var viewController: AddEntryViewController?
    init(viewController: AddEntryViewController) {
        self.viewController = viewController
    }
}

extension AddEntryRouter: AddEntryRoutingLogic {
    func navigateToParentWithUpdate() {
        if let destinationVC = viewController?.navigationController?.previuosViewController as? EntryListViewController {
            var destinationDS = destinationVC.presenter.dataStore
            destinationDS.needUpdate = true
        }
        viewController?.navigationController?.popViewController(animated: true)
    }
}
