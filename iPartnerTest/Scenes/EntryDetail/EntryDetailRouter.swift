//
//  EntryDetailRouter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 05/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation

protocol EntryDetailRoutingLogic {
    func routeToParent()
}

class EntryDetailRouter {
    weak var viewController: EntryDetailViewController?
    init(viewController: EntryDetailViewController) {
        self.viewController = viewController
    }
}

extension EntryDetailRouter: EntryDetailRoutingLogic {
    func routeToParent() {
        viewController?.dismiss(animated: true)
    }
}

