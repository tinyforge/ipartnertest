//
//  EntryDetailPresenter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 04/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit


protocol EntryDetailBusinessLogic {
    var dataStore: EntryDetailDataStore { get }
    func fetchEntryToDisplay()
}

protocol EntryDetailDataStore {
    var entryText: String? { get set }
}

class EntryDetailPresenter: EntryDetailDataStore {
    weak var view: EntryDetailDisplayLogic?
    var router: EntryDetailRoutingLogic
    var entryText: String?
    
    init(view: EntryDetailDisplayLogic, router: EntryDetailRoutingLogic) {
        self.view = view
        self.router = router
    }
}

extension EntryDetailPresenter: EntryDetailBusinessLogic {
    var dataStore: EntryDetailDataStore {
        get { return self }
    }
    
    func fetchEntryToDisplay() {
        view?.showEntry(text: entryText ?? "")
    }
}
