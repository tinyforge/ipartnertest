//
//  EntryDetailViewController.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 04/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol EntryDetailDisplayLogic: class {
    func showEntry(text: String)
}

class EntryDetailViewController: UIViewController {
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        }
    }
    var presenter: EntryDetailBusinessLogic!
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    func setup() {
        let router = EntryDetailRouter(viewController: self)
        presenter = EntryDetailPresenter(view: self, router: router)
    }
    
    // MARK: View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Detail"
        presenter.fetchEntryToDisplay()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textView.setContentOffset(.zero, animated: false)
    }
}


extension EntryDetailViewController: EntryDetailDisplayLogic {
    func showEntry(text: String) {
        textView.text = text
    }
}
