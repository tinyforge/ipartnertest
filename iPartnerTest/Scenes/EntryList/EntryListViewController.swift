//
//  EntriesListViewController.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol EntryListDisplayLogic: NSObjectProtocol {
    func startLoading()
    func stopLoading()
    func refreshEntryList()
    func displayAlertView(message: String, buttonText: String)
}

class EntryListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.tableFooterView = UIView()
        }
    }
    var alertView = AlertView()
    var activitiIndicatorView = ActivityIndicatorView()
    var presenter: EntryListBusinessLogic!
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    func setup() {
        let router = EntryListRouter(viewController: self)
        presenter = EntryListPresenter(view: self, router: router)
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "List"
        presenter.fetchEntries()
    }
    
    // MARK: Actions
    @IBAction func addButtonPressed(_ sender: Any) {
        presenter.addButtonPressed()
    }
}

extension EntryListViewController: EntryListDisplayLogic {
    func displayAlertView(message: String, buttonText: String) {
        alertView.messageLabel.text = message
        alertView.actionButton.setTitle(buttonText, for: .normal)
        alertView.show(in: view)
        tableView.isHidden = true
    }
    
    func refreshEntryList() {
        tableView.reloadData()
        tableView.setContentOffset(.zero, animated: false)
        tableView.isHidden = false
        alertView.remove()
    }
    
    func startLoading() {
        activitiIndicatorView.show(in: self.view)
    }
    
    func stopLoading() {
        activitiIndicatorView.hide()
    }
}

extension EntryListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.entriesCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! EntryListCell
        presenter.configure(cell: cell, for: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelect(row: indexPath.row)
    }
}

extension EntryListViewController: AlertViewDelegate {
    func buttonTap(_ sender: UIButton) {
        presenter.fetchEntries()
    }
}
