//
//  EntriesListPresenter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 03/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol EntryListBusinessLogic {
    var dataStore: EntryListDataStore { get }
    var entriesCount: Int { get }
    func fetchEntries()
    func configure(cell: EntryListCellDisplayLogic, for row: Int)
    func addButtonPressed()
    func didSelect(row: Int)
}

protocol EntryListDataStore {
    var needUpdate: Bool { get set }
}

class EntryListPresenter: EntryListDataStore {
    weak var view: EntryListDisplayLogic!
    var router: EntryListRoutingLogic
    var entries = [Entry]()
    var needUpdate = true
    
    init(view: EntryListDisplayLogic, router: EntryListRouter) {
        self.view = view
        self.router = router
    }
}

extension EntryListPresenter: EntryListBusinessLogic {
    var dataStore: EntryListDataStore {
        get { return self }
    }
    
    func didSelect(row: Int) {
        router.presentDetailView(with: entries[row].text)
    }
    
    func addButtonPressed() {
        router.presentAddEntryView()
    }
    
    var entriesCount: Int {
        return entries.count
    }
    
    func configure(cell: EntryListCellDisplayLogic, for row: Int) {
        let entry = entries[row]
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        // created
        let createdDateString = dateFormatter.string(from: entry.createdAt)
        cell.display(createdDate: "Created: \(createdDateString)")
        
        // updated
        if entry.createdAt != entry.updatedAt {
            let updatedDateString = dateFormatter.string(from: entry.updatedAt)
            cell.display(updatedDate: "Updated: \(updatedDateString)")
        } else {
            cell.hideUpdatedDate()
        }
        
        // body
        let bodyText = (entry.text.count > 200) ? "\(entry.text.prefix(200))..." : entry.text
        cell.display(bodyText: bodyText)
    }
    
    func fetchEntries() {
        if !needUpdate { return }
        view?.startLoading()
        entries.removeAll()
        self.view?.refreshEntryList()
        let apiManager = APIManager(session: User.shared.sessionId)
        apiManager.getEntries { [weak self] result in
            guard let self = self else { return }
            self.view?.stopLoading()
            switch result {
            case .success(let entries):
                self.entries = entries
                self.view?.refreshEntryList()
                self.needUpdate = false
            case .failure(let error):
                self.view?.displayAlertView(message: error.localizedDescription, buttonText: "Обновить данные")
            }
        }
    }
    
    
}
