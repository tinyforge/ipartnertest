//
//  EntryListCell.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 04/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol EntryListCellDisplayLogic: NSObjectProtocol {
    func display(createdDate: String)
    func display(updatedDate: String)
    func display(bodyText: String)
    func hideUpdatedDate()
}

class EntryListCell: UITableViewCell {
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
}

extension EntryListCell: EntryListCellDisplayLogic {
    func display(createdDate: String) {
        createdLabel.text = createdDate
    }

    func display(updatedDate: String) {
        updatedLabel.isHidden = false
        updatedLabel.text = updatedDate
    }

    func display(bodyText: String) {
        bodyLabel.text = bodyText
    }
    
    func hideUpdatedDate() {
        updatedLabel.isHidden = true
    }
}
