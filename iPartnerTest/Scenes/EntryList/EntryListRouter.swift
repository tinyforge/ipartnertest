//
//  EntryListRouter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 04/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation

protocol EntryListRoutingLogic: NSObjectProtocol {
    func presentDetailView(with entryText: String)
    func presentAddEntryView()
}

class EntryListRouter: NSObject {
    weak var viewController: EntryListViewController?
    init(viewController: EntryListViewController) {
        self.viewController = viewController
    }
}

extension EntryListRouter: EntryListRoutingLogic {
    func presentDetailView(with entryText: String) {
        let destinationVC = EntryDetailViewController.storyboardInstance()
        var destinationDS = destinationVC.presenter.dataStore
        destinationDS.entryText = entryText
        viewController?.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    func presentAddEntryView() {
        let addEntryViewController = AddEntryViewController.storyboardInstance()
        viewController?.navigationController?.pushViewController(addEntryViewController, animated: true)
    }
}
