//
//  File.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 08/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation
import UIKit

protocol LaunchRoutingLogic {
    func routeToStartScreen()
}

class LaunchRouter {
    weak var viewController: LaunchViewController?
    init(viewController: LaunchViewController?) {
        self.viewController = viewController
    }
}

extension LaunchRouter: LaunchRoutingLogic {
    func routeToStartScreen() {
        let mainViewController = EntryListViewController.storyboardInstance()
        let rootViewController = UINavigationController.init(rootViewController: mainViewController)
        let keyWindow: UIWindow = (UIApplication.shared.keyWindow)!
        UIView.transition(with: keyWindow, duration: 0.3, options: .transitionCrossDissolve, animations: {
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            keyWindow.rootViewController = rootViewController
            UIView.setAnimationsEnabled(oldState)
        })
    }
}
