//
//  LaunchViewController.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 08/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import UIKit

protocol LaunchDisplayLogic: class {
    func displayLoadingContent()
    func displayAlertView(message: String, buttonText: String)
}

class LaunchViewController: UIViewController {
    var presenter: LaunchBussinesLogic!
    var alertView = AlertView()
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    func setup() {
        let router = LaunchRouter(viewController: self)
        presenter = LaunchPresenter(view: self, router: router)
    }
    
    override func viewDidLoad() {
        alertView.delegate = self
        activityIndicatorView.hidesWhenStopped = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        presenter.loadAppliaction()
    }
}

extension LaunchViewController: LaunchDisplayLogic {
    func displayLoadingContent() {
        activityIndicatorView.startAnimating()
        alertView.remove()
        contentView.isHidden = false
    }
    
    func displayAlertView(message: String, buttonText: String) {
        contentView.isHidden = true
        alertView.messageLabel.text = message
        alertView.actionButton.setTitle(buttonText, for: .normal)
        alertView.show(in: view)
    }
}

extension LaunchViewController: AlertViewDelegate {
    func buttonTap(_ sender: UIButton) {
        presenter.loadAppliaction()
    }
}
