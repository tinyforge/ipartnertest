//
//  LaunchPresenter.swift
//  iPartnerTest
//
//  Created by Dmitriy Polyakov on 08/02/2019.
//  Copyright © 2019 Dmitriy Polyakov. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

protocol LaunchBussinesLogic {
    func loadAppliaction()
}

class LaunchPresenter {
    weak var view: LaunchDisplayLogic?
    var router: LaunchRoutingLogic?
    init(view: LaunchDisplayLogic, router: LaunchRoutingLogic) {
        self.view = view
        self.router = router
    }
}

extension LaunchPresenter: LaunchBussinesLogic {
    func loadAppliaction() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
//        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
//        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized
//        IQKeyboardManager.shared.toolbarTintColor = Color.primary.auto
//        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        
        view?.displayLoadingContent()
        if User.shared.sessionId.isEmpty {
            APIManager(session: "").createNewSession() { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let sessionId):
                    User.shared.sessionId = sessionId
                    self.router?.routeToStartScreen()
                case .failure(let error):
                    self.view?.displayAlertView(message: error.localizedDescription, buttonText: "Попробовать еще раз")
                    return
                }
            }
        } else {
            router?.routeToStartScreen()
        }
    }
}
